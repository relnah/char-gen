# Char Gen

Simple character generator/calculator. Primarily for classic PnP games.

# Supported games

- Drakar & Demoner
  - Basic BP cost calculator
- Dungeons & Dragons
  - Early stages of development

# Development

## Prerequisities

- Yarn 1.22.x
- Node 15

## Project setup

```
yarn install
```

### Compiles and hot-reloads for development

```
yarn serve
```

### Compiles and minifies for production

```
yarn build
```

### Lints and fixes files

```
yarn lint
```
