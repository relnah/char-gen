FROM node:15-slim AS builder

# make the 'app' folder the current working directory
WORKDIR /app

# copy both 'package.json'
COPY package.json ./
COPY yarn.lock ./

# install project dependencies
RUN yarn install

# copy project files and folders to the current working directory (i.e. 'app' folder)
COPY . .

# build app for production with minification
RUN yarn run build


FROM node:15-slim

WORKDIR /app

COPY --from=builder /app/dist .

# install simple http server for serving static content
RUN yarn global add http-server

EXPOSE 8080
CMD [ "http-server", "./" ]
